TARGET := split_client_hello
SOURCES := split_client_hello.c
SYSTEMD := /etc/systemd/system
SERVICE := splitClientHello.service
BIN := /usr/local/bin
IPTABLES := /etc/iptables/iptables.rules

all: $(TARGET)

$(TARGET): $(SOURCES)
	gcc -o $@ $^ -lnetfilter_queue

install: $(BIN)/$(TARGET) $(SYSTEMD)/$(SERVICE)
	@echo "installing..."

$(SYSTEMD)/$(SERVICE): $(SERVICE)
	install -m644 $< $@
	systemctl daemon-reload

$(BIN)/$(TARGET): $(TARGET)
	install $< $@

load: iptables_on $(SYSTEMD)/$(SERVICE)
	systemctl start $(SERVICE)

unload: iptables_off $(SYSTEMD)/$(SERVICE)
	systemctl stop $(SERVICE)

iptables_on:
	@f=$(notdir $(IPTABLES)); \
	if ! diff $$f $(IPTABLES) > /dev/null; then \
		install -m644 $$f $(IPTABLES); \
	fi
	systemctl restart iptables.service

iptables_off:
	systemctl stop iptables.service

clean:
	rm -f *.o $(TARGET)
