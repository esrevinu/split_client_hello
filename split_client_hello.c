#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>    /* for NF_DROP */
#include <errno.h>
#include <string.h>

#include <linux/tcp.h>
#include <linux/ip.h>

#include <sys/socket.h>

#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnetfilter_queue/libnetfilter_queue_ipv4.h>
#include <libnetfilter_queue/libnetfilter_queue_tcp.h>
#include <libnetfilter_queue/pktbuff.h>

#ifdef DEBUG
#define dprintf(...) do { printf(__VA_ARGS__); } while (0)
#else
#define dprintf(...) do { } while (0)
#endif

int raw_socket;

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
              struct nfq_data *nfa, void *d)
{
  int id;
  int ret;
  struct nfqnl_msg_packet_hdr *ph;
  struct pkt_buff *pktb = NULL;
  struct iphdr *ip_header;
  struct tcphdr *tcp_header;
  unsigned char *data;
  unsigned char *payload;
  unsigned int payload_len;
  char buf[4096];
  unsigned int first_fragment_len = 2;
  struct sockaddr_in address;

  ph = nfq_get_msg_packet_hdr(nfa);
  if (ph) {
    id = ntohl(ph->packet_id);
  }

  ret = nfq_get_payload(nfa, &data);
  if (ret <= 0) goto fail;

  pktb = pktb_alloc(AF_INET, data, /*packet length*/ ret, 0);
  if (!pktb) goto fail;

  ip_header = nfq_ip_get_hdr(pktb);
  if (!ip_header) goto fail;
  nfq_ip_set_transport_header(pktb, ip_header);
  tcp_header = nfq_tcp_get_hdr(pktb);
  if (!tcp_header) goto fail;

  dprintf("ip: version=%d saddr=0x%08x daddr=0x%08x proto=%d\n",
          ip_header->version, ip_header->saddr,
          ip_header->daddr, ip_header->protocol);
  dprintf("tcp: sport=%u dport=%u seq=%u ack=%u\n",
          ntohs(tcp_header->source), ntohs(tcp_header->dest),
          ntohs(tcp_header->seq), ntohs(tcp_header->ack));

  payload = nfq_tcp_get_payload(tcp_header, pktb);
  payload_len
    = nfq_tcp_get_payload_len(tcp_header, pktb) - tcp_header->doff * 4;
  dprintf("payload length = %u content_type = 0x%02x tls = 0x%02x%02x\n",
          payload_len, *payload, *(payload+1), *(payload+2));

  memcpy(buf, payload, payload_len);

  address.sin_family = AF_INET;
  address.sin_port = 0;
  address.sin_addr.s_addr = ip_header->daddr;

  /* first fragment */
  nfq_tcp_mangle_ipv4(pktb, 0, payload_len, buf, first_fragment_len);
  ret = sendto(raw_socket, pktb_data(pktb), pktb_len(pktb), 0x0,
               (const struct sockaddr *)&address, sizeof(address));
  if (ret == -1) fprintf(stderr, "first sendto error");

  /* second fragment */
  tcp_header->seq = htonl(ntohl(tcp_header->seq) + first_fragment_len);
  nfq_tcp_mangle_ipv4(pktb, 0, payload_len, buf + first_fragment_len,
                      payload_len - first_fragment_len);
  ret = sendto(raw_socket, pktb_data(pktb), pktb_len(pktb), 0x0,
               (const struct sockaddr *)&address, sizeof(address));
  if (ret == -1) fprintf(stderr, "second sendto error");

  pktb_free(pktb);
  return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);

 fail:
  if (pktb) pktb_free(pktb);
  return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

int main(int argc, char **argv)
{
  struct nfq_handle *h;
  struct nfq_q_handle *qh;
  int fd;
  int rv;
  uint32_t queue = 0;
  char buf[4096] __attribute__ ((aligned));
  int on = 1;

  if (argc == 2) {
    queue = atoi(argv[1]);
    if (queue > 65535) {
      fprintf(stderr, "Usage: %s [<0-65535>]\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  raw_socket = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);
  setsockopt(raw_socket, IPPROTO_IP, IP_HDRINCL, (char *)&on, sizeof(on));

  dprintf("opening library handle\n");
  h = nfq_open();
  if (!h) {
    fprintf(stderr, "error during nfq_open()\n");
    exit(1);
  }

  dprintf("unbinding existing nf_queue handler for AF_INET (if any)\n");
  if (nfq_unbind_pf(h, AF_INET) < 0) {
    fprintf(stderr, "error during nfq_unbind_pf()\n");
    exit(1);
  }

  dprintf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
  if (nfq_bind_pf(h, AF_INET) < 0) {
    fprintf(stderr, "error during nfq_bind_pf()\n");
    exit(1);
  }

  dprintf("binding this socket to queue '%d'\n", queue);
  qh = nfq_create_queue(h, queue, &cb, NULL);
  if (!qh) {
    fprintf(stderr, "error during nfq_create_queue()\n");
    exit(1);
  }

  dprintf("setting copy_packet mode\n");
  if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
    fprintf(stderr, "can't set packet_copy mode\n");
    exit(1);
  }

  dprintf("setting flags to request UID and GID\n");
  if (nfq_set_queue_flags(qh, NFQA_CFG_F_UID_GID, NFQA_CFG_F_UID_GID)) {
    fprintf(stderr, "This kernel version does not allow to "
            "retrieve process UID/GID.\n");
  }

  dprintf("setting flags to request security context\n");
  if (nfq_set_queue_flags(qh, NFQA_CFG_F_SECCTX, NFQA_CFG_F_SECCTX)) {
    fprintf(stderr, "This kernel version does not allow to "
            "retrieve security context.\n");
  }

  dprintf("Waiting for packets...\n");

  fd = nfq_fd(h);

  for (;;) {
    if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
      dprintf("pkt received\n");
      nfq_handle_packet(h, buf, rv);
      continue;
    }
    /* if your application is too slow to digest the packets that
     * are sent from kernel-space, the socket buffer that we use
     * to enqueue packets may fill up returning ENOBUFS. Depending
     * on your application, this error may be ignored. Please, see
     * the doxygen documentation of this library on how to improve
     * this situation.
     */
    if (rv < 0 && errno == ENOBUFS) {
      dprintf("losing packets!\n");
      continue;
    }
    perror("recv failed");
    break;
  }

  dprintf("unbinding from queue 0\n");
  nfq_destroy_queue(qh);

  dprintf("closing library handle\n");
  nfq_close(h);

  exit(0);
}

