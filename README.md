리눅스에서 SNI 차단 우회
==============================

# 참고한 것들
* [FuckDPI](https://github.com/prodeveloper0/FuckDPI_V2)
* [gilgil's blog](https://gilgil.gitlab.io/2019/02/11/1.html)
* [nfq-examples](https://github.com/azat-archive/nfq-examples/blob/master/nfqnl_rst_fin.c)
* [hackerschool](http://research.hackerschool.org/study/SS_1.htm)

# iptables 규칙

잘 모르는 관계로 
[여기](https://blog.g3rt.nl/take-down-sslv3-using-iptables.html)를 참조하여
만들었다. 이 규칙은 HTTPS client hello 패킷을 처리하도록 NFQUEUE로 보낸다.

```
# iptables -I OUTPUT 1 \
  -p tcp \! -f \
  -m state --state ESTABLISHED \
  -m connbytes --connbytes 100:1000 --connbytes-dir original \
  --connbytes-mode bytes \
  -m u32 --u32 \
  "0>>22&0x3C@ 12>>26&0x3C@ 0 >> 24=0x16 && \
   0>>22&0x3C@ 12>>26&0x3C@ 2 & 0xFF=0x01" \
  -j NFQUEUE --queue-num 0 --queue-bypass
```

이렇게 실행하고 나서 규칙을 저장했다가 리부팅할 때 복원하게 하려면
다음과 같이 한다.

```
# iptables-save > /etc/iptables/iptables.rules
# systemctl enable iptables.service
```

# split_client_hello 프로그램 빌드
이 프로그램은 libnetfilter_queue를 이용해서 패킷을 쪼개고 raw socket을
이용해서 보낸다. Netfilter로 다시 inject하는 방법을 몰라서 queue에 온 패킷은
drop한다. Netfilter에 새 패킷을 inject하는 방법이 없는 것 같다.

```
$ make
```

디버그를 하려면
```
$ gcc -o split_client_hello split_client_hello -lnetfilter_queue -DDEBUG
```

# split_client_hello 프로그램 설치

```
$ sudo make install
```
위 명령을 실행하면 실행파일을 `/usr/bin`에 복사하고
systemd service 파일을 `/etc/systemd/system`에 복사한다.

아래 명령으로 splitClientHello 서비스를 부팅시에 시작하게 하고 지금 시작시킨다.

```
$ sudo systemctl start splitClientHello.service
$ sudo systemctl enable splitClientHello.service
```

# 개선해야 할 점

* ipv6 지원
* 쪼갠 패킷이 iptables 규칙에 걸리지 않게 할 좀더 세련된 방법 찾기.
  지금은 첫 조각의 payload를 2 바이트로 해서 u32 필터에 걸리지 않음.

